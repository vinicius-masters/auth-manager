from os import environ
from pyjung import JungRegistry, JungTasker


class JungAuth(JungRegistry):

    def __init__(self, kafka_broker, in_topic, out_topic, group, port='9092'):

        super().__init__(kafka_broker, in_topic, out_topic, port, group)
        self.user_tasker = JungTasker(in_topic="user_results",
                                      out_topic="user_tasks",
                                      ip=kafka_broker, port=port,
                                      group=group)

    def process_message(self, message):
        task_id = message["id"]
        task = message["task"]
        username = message["content"]["username"]
        password = message["content"]["password"]
        device_id = message["content"]["device_id"]

        response = {"task_id": task_id, "task": task}
        result = {"auth": False}

        if task == "AUTH":
            # create task to add device_id to user
            task_obj = {"username": username}
            get_user = self.tasker.create_task("GET", task_obj)

            self.user_tasker.publish(get_user)
            task_result = self.user_tasker.get_task_result(get_user["id"])

            if task_result is not None and "error" not in task_result:
                if password == task_result["password"]:
                    result["auth"] = True
                    if device_id is not None:
                        if device_id not in task_result["devices"]:
                            result["auth"] = False

        response["result"] = result
        return response


def main():
    kafka_broker = environ["KAFKA_BROKER"]
    auth_manager = JungAuth(kafka_broker=kafka_broker,
                            in_topic="auth_tasks",
                            out_topic="auth_results",
                            port="9092",
                            group="auth-manager")
    print("starting auth manager...")
    auth_manager.process_tasks()


if __name__ == "__main__":
    main()
