#!/bin/bash

DEFAULT_KAFKA="192.168.0.100"
ARG1=${1:-KAFKA_BROKER=$DEFAULT_KAFKA}

sudo capstan run -e '--env='$ARG1' /python.so /auth-manager/jung_auth.py' -n bridge
